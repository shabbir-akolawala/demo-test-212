#!/usr/local/bin/python3
import urllib.request, json 

# URL variables
json_url_1 = "https://horizon.stellar.org"
json_url_2 = "https://horizon.stellar.org"
# Other constants
metric_name = "bash_core_version_ismatch"

def printPromQLMetric(isMatch):
    print("%s{job=\"core-version-compare\",verb=\"isMatch\", script1=\"horizon.stellar.org\", script2=\"horizon-testnet.stellar.org\"} %s" % (metric_name , str(isMatch)) )


# Print metrics headers
print("# HELP %s compares core_version in json file, if same returns 1, if different returns 0" % metric_name)
print("# TYPE %s gauge" % metric_name)

# For demo, looping for 5 time. For real script, some meaningful assertion need to be in place
for i in range(0,4):
    with urllib.request.urlopen(json_url_1) as url1:
        data = json.loads(url1.read().decode())
        v1=data["core_version"]

    with urllib.request.urlopen(json_url_2) as url2:
        data = json.loads(url2.read().decode())
        v2=data["core_version"]

    if v1 == v2:
        printPromQLMetric(1)
    else:
        printPromQLMetric(0)
