#!/bin/bash

# I assume, jq is install on the machine which runs the script. 
# If not, then need to do with grep, cut, tr or sed
# Better would be write python script

# URLs variable
URL1="https://horizon.stellar.org"
URL2="https://horizon-testnet.stellar.org"

METRIC_NAME="bash_core_version_ismatch"

printPromQLMetric(){
    echo "$METRIC_NAME{job=\"core-version-compare\",verb=\"isMatch\", script1=\"horizon.stellar.org\", script2=\"horizon-testnet.stellar.org\"} $1"
}

# Print metrics headers
echo "# HELP $METRIC_NAME compares core_version in json file, if same returns 1, if different returns 0"
echo "# TYPE $METRIC_NAME gauge"

# For demo, looping for 5 time. For real script, some meaningful assertion need to be in place
for (( i=0; i<5; ++i)); do
    # Curl to get the core_version 
    v1=$(curl -s $URL1 | jq '.core_version')

    # Curl to get the core_version
    v2=$(curl -s $URL2 | jq '.core_version')

    if [[ "$v1" == "$v2" ]]; then
        printPromQLMetric 1
    else
        printPromQLMetric 0
    fi
done

